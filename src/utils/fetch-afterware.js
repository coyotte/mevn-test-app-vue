export const logout = function() {
  this.$cookies.remove("tkn");
  this.$store.commit("removeUser");
  this.$router.replace("/login");
};

export const unauthorizedCheck = function(response) {
  if (response.status === 401) {
    logout.call(this);
  }
};
